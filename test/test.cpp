/* 
 * File:   test.cpp
 * Author: Alexander-i7
 *
 * Created on den 6 maj 2012, 11:25
 */

#include <cstdlib>
#include <iostream>
#include "../src/SecureString.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    const int BUFFSIZE = 50;
    SecureString sec1(BUFFSIZE);
	SecureString sec2("0000", 0, false);
	SecureString sec3(sec2);
	SecureString sec4;
    
    char* inBuff1 = "AAAAAAAAAAAAAAAAAAAA";
    char* inBuff2 = "BBBBB";
    char* inBuff3 = "C";
    char* inBuff4 = "";
    char* inBuff5 = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE\0EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE";

	const int SIZE = 10000;
	char* largeBuff = new char[SIZE];
	for(int i=0; i<SIZE; i++){
		largeBuff[i] = 'A' + (i%26);
	}
	largeBuff[SIZE-1] = '\0';
	SecureString* large = new SecureString(largeBuff);
	char* modifiedcopy = large->getUnsecureStringM();
	modifiedcopy[SIZE-2] = '-';
	large->UnsecuredStringFinished();
	cout<<large->getUnsecureString()<<endl;
	large->UnsecuredStringFinished();
    
	sec1.assign(inBuff1, 1, false);
	sec2.append(inBuff2, 0, false);
	sec2.append(sec3);
	sec2.append(sec2);
	sec4.append(inBuff5, 0, false);
    
    cout<<"Your strings is now in secure storage, is this your strings?\n";
	cout<<"sec1: "<<sec1.getUnsecureString()<<"\nThe string is "<<sec1.length()<<" chars long!\n";
	cout<<"sec2: "<<sec2.getUnsecureString()<<"\nThe string is "<<sec2.length()<<" chars long!\n";
	cout<<"sec3: "<<sec3.getUnsecureString()<<"\nThe string is "<<sec3.length()<<" chars long!\n";
	cout<<"sec4: "<<sec4.getUnsecureString()<<"\nThe string is "<<sec4.length()<<" chars long!\n";
    
    sec1.UnsecuredStringFinished();//delete the unsecured copy!
    sec2.UnsecuredStringFinished();//delete the unsecured copy!
    sec3.UnsecuredStringFinished();//delete the unsecured copy!
    sec4.UnsecuredStringFinished();//delete the unsecured copy!
    
    cin.get();

    return 0;
}

